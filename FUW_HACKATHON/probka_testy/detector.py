import numpy as np
from scipy import misc
import cv2
def main():
    # xFilter =np.array([[1/16,1/8,1/16],[1/8,1/4,1/8],[1/16,1/8,1/16]])
    yFilter = np.array([[1,2,1],[0, 0, 0],[-1, -2, -1]])
    xFilter = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
    print(xFilter[0])
    print(yFilter)
    print(xFilter.shape)
    face = misc.face(True)
    # misc.imsave('img.jpg', face) # First we need to create the PNG file
    face = misc.imread('filter.jpg')
    print(face.shape[0])
    edgeX = np.zeros(shape = face.shape)
    edgeY = np.zeros(shape = face.shape)
    edgeXY = np.zeros(shape = face.shape)
    print(edgeX.shape)
    for i in range(1,face.shape[0]-1):
         for j in range(1,face.shape[1]-1):
           edgeX[i,j] = face[i-1,j-1]*xFilter[0,0] + face[i,j-1]*xFilter[1,0] + face[i+1,j-1]*xFilter[2,0] 
           + face[i-1,j]*xFilter[0,1] + face[i,j]*xFilter[1,1] + face[i+1,j]*xFilter[2,1] 
           + face[i-1,j+1]*xFilter[0,2] + face[i,j+1]*xFilter[1,2] + face[i+1,j+1]*xFilter[2,2]
    misc.imsave('edgeX2.jpg',edgeX)
    for i in range(1,face.shape[0]-1):
         for j in range(1,face.shape[1]-1):
           edgeY[i,j] = face[i-1,j-1]*yFilter[0,0] + face[i,j-1]*yFilter[1,0] + face[i+1,j-1]*yFilter[2,0] 
           + face[i-1,j]*yFilter[0,1] + face[i,j]*yFilter[1,1] + face[i+1,j]*yFilter[2,1] 
           + face[i-1,j+1]*yFilter[0,2] + face[i,j+1]*yFilter[1,2] + face[i+1,j+1]*yFilter[2,2]
    misc.imsave('edgeY2.jpg',edgeY)
    edgeXY = np.sqrt(np.power(edgeX,2) + np.power(edgeY,2))
    misc.imsave('edgeXY2.jpg',edgeXY)
    # river = misc.imread('edgeY.jpg')
    # x = 160
    # river[river<x] = 0
    # river[river>x] = 255
    # print(river)
    # misc.imsave('filter.jpg',river)
if __name__ == '__main__':
    main()