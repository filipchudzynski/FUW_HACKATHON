import cv2
import numpy as np
from matplotlib import pyplot as plt
def main():
    img = cv2.imread('melting_ice.jpg',1)
    lab= cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    # cv2.imshow("lab",lab)
    img=img[:,:]
    # img[img>150]=1
    # img[img<100]=0
    cv2.imshow("image",img)
    cv2.waitKey(10000)
    l, a, b = cv2.split(lab)
    # cv2.imshow('l_channel', l)
    # cv2.waitKey(10000)
    # cv2.imshow('a_channel', a)
    # cv2.waitKey(10000)
    # cv2.imshow('b_channel', b)
    # cv2.waitKey(10000)

    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
    cl = clahe.apply(l)
    # cv2.imshow('CLAHE output', cl)
    # cv2.waitKey(10000)

    #-----Merge the CLAHE enhanced L-channel with the a and b channel-----------
    limg = cv2.merge((cl,a,b))
    # cv2.imshow('limg', limg)
    # cv2.waitKey(10000)

    #-----Converting image from LAB Color model to RGB model--------------------
    final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    cv2.imshow('final', final)
    cv2.waitKey(10000)
    #hsv = cv2.cvtColor(final, cv2.COLOR_BGR2HSV)
    upper_blue = np.array([0,0,0])
    lower_blue = np.array([255,255,255])
    
    mask = cv2.inRange(final,lower_blue, upper_blue)
    res = cv2.bitwise_and(final,final, mask= mask)
    
    hi = 50
    edges = cv2.Canny(final,hi/3,hi)
    cv2.imshow('res', edges)
    cv2.waitKey(10000)

if __name__ == '__main__':
    main()