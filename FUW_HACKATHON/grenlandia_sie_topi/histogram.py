import cv2
import numpy as np
from matplotlib import pyplot as plt
import PIL
from PIL import ImageFilter
def main():
    img = cv2.imread('melting_ice.jpg',0)
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
    #dif = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
    dif = cv2.dilate(img, kernel, iterations=5)
    
    cv2.imwrite('dillation.jpg', dif)
    
    # plt.plot(median)
    # plt.show()
    # for i,col in enumerate(color):
    #     histr = cv2.calcHist([img],[i],None,[256],[0,256])
    #     
    #     plt.xlim([0,256])
    #     plt.show()

def function():
    # img = PIL.Image.open("melting_ice_b_w.jpg")
    # x = 3
    # box = img.filter(ImageFilter.BoxBlur(x))
    # box.save("boxBlur2.jpg", "JPEG")
    # max = img.filter(ImageFilter.MaxFilter(x))
    # max.save("maxBlur2.jpg", "JPEG")
    # min = img.filter(ImageFilter.MinFilter(x))
    # min.save("minBlur2.jpg", "JPEG")
    # mode = img.filter(ImageFilter.ModeFilter(x))
    # mode.save("modeBlur2.jpg", "JPEG")
    img = cv2.imread('melting_ice_b_w.jpg',0)
    #dif = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
    # kernel = cv2.getStructuringElement(cv2.MORPH_OPEN, (2,2))
    kernel = np.ones((2,2),np.uint8)
    dif = cv2.dilate(img, kernel, iterations=15)
    canny = cv2.Canny(dif,255/4,200)
    close = cv2.morphologyEx(canny, cv2.MORPH_CLOSE, np.array([[255,255],[255,255]]))
    cv2.imwrite('close.jpg', canny)
if __name__ == '__main__':
    function()    